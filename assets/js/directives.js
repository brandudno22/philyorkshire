(function(){

	//website header directive

	angular.module('philYorkshire').directive("websiteHeader", function() {
		return {
			restrict: 'E',
			templateUrl : "./views/directives/website-header.html"
		};
	});

	//website footer directive

	angular.module('philYorkshire').directive("websiteFooter", function() {
		return {
			restrict: 'E',
			templateUrl : "./views/directives/website-footer.html"
		};
	});


	// flexslider directive

	angular.module('philYorkshire').directive('flexsliderInit', function ($timeout) {
	  return {
	    link: function (scope, element) {
	        $timeout(function() {
		        $(element).flexslider({
					directionNav: false
	        	});
	        }, 100);
	    }
	  }
	});

	// add active class to navbar

	angular.module('philYorkshire').directive('isActiveNav', [ '$location', function($location) {
	return {
	 restrict: 'A',
	 link: function(scope, element) {
	   scope.location = $location;
	   scope.$watch('location.path()', function(currentPath) {
		   //console.log(currentPath + "+" + element[0].attributes['href'].nodeValue);
		   var thisPath = currentPath;
		   //console.log(thisPath);
	     if(currentPath === element[0].attributes['href'].nodeValue) {
	       element.addClass('active');
		 } else if (thisPath.indexOf('work') > -1) {
			if ('/work' === element[0].attributes['href'].nodeValue) {
				element.addClass('active');
			} else {
				element.removeClass('active');
			}
		 } else {
	       element.removeClass('active');
	     }
	   });
	 }
	 };
	}]);

	// contact page heights

	angular.module('philYorkshire').directive('equalHeights', function () {
		return {

			link: function() {

			    function contactHeight() {

				    var leftheight = $('.left').outerHeight(),
								windowHeight = $(window).outerHeight(),
								footerHeight = $('#container footer').outerHeight();
								headerHeight = $('#container header').outerHeight();



						$('.contact-page').css({'min-height': windowHeight - (footerHeight + headerHeight) + 1});

				    if ($(window).width() >= 768) {

							$('.right').css({'height': leftheight});

						} else {

							$('.right').css({'height': 'auto'});

						}

				}

				contactHeight();

				$(window).resize(function(){
					contactHeight();
				});

				$(document).ajaxComplete(function() {
				contactHeight();
				});

			}

		};
	});



	// waypoints OS-animation directive

	angular.module('philYorkshire').directive('osAnimation', function ($timeout) {
		return {
			link: function() {

				// on load move body 1px down in order to activate waypoints
			    $('body,html').animate({
					scrollTop: $(window).scrollTop() + 1
				}, 10);

			    // waypoints and animate.css

			    var scrolling = false;
				function onScrollInit( items, trigger){
					// code from above
					items.each( function() {
				    var osElement = $(this),
				        osAnimationClass = osElement.attr('data-os-animation');

				        if(osElement.attr('data-os-animation-delay')) {
					        var osAnimationDelay = osElement.attr('data-os-animation-delay');
				        } else {
					    	var osAnimationDelay = Math.random()+'s';
				        }




				        osElement.css({
				          '-webkit-animation-delay':  osAnimationDelay,
				          '-moz-animation-delay':     osAnimationDelay,
				          'animation-delay':          osAnimationDelay
				        });

				        var osTrigger = ( trigger ) ? trigger : osElement;

				        osTrigger.waypoint(function() {
				          osElement.addClass('animated').addClass(osAnimationClass);
				          },{
				              triggerOnce: true,
				              offset: '100%'
				        });
				    });
				}

				$(window).on('scroll', function(){
					if(!scrolling){
						onScrollInit( $('.os-animation') );
						onScrollInit( $('.staggered-animation'), $('.staggered-animation-container') );
						scrolling = true;
					}
				});

			}
		}
	});


})();
