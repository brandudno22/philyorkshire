jQuery(document).ready(function($) {
	
	// mobile menu scripts
	
	$('.mobile-toggle .fa').click(function(){
		$(this).parent().toggleClass('active');
		
		$('nav.main-nav').slideToggle();
		
	});
	
/*
	$('.hover a').bind('touchstart touchend', function(e) {
		e.preventDefault();
        $(this).parent('.work-item').toggleClass('hover_effect');
    });
*/
	
	// footer reveal scripts
	
	$('footer .footer-reveal').click(function(){
		
		$(this).toggleClass('open');
		$(this).siblings('.footer-nav').slideToggle();
		$(this).children('.fa').toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
		
		$('html, body').animate({
	        scrollTop: $('.footer-nav').offset().top
	    }, 10);
		
	});
	
	// insert year dynamically into footer
	
	var theYear = (new Date).getFullYear();
    $('#year').html(theYear);
		
});