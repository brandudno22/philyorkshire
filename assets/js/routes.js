(function(){
	
	angular.module('philYorkshire').config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$routeProvider
	    .when('/', {
	        templateUrl : "views/home.html",
	        controller: "homeController",
	        controllerAs: "home"
	    })
	    .when('/work',{
			templateUrl: "views/work.html",
			controller: "workController",
	    })
	    .when('/work/:slug',{
		 	templateUrl : "views/single-work.html",
		 	controller: 'singleWork',
		 	controllerAs: 'work'
	    })
	    .when('/about',{
			templateUrl: "views/about.html"
	    })
	    .when('/blog',{
			template: "<p>Blog page</p>"
	    })
	    .when('/contact',{
			templateUrl: "views/contact.html",
			controller: 'contactController',
	    })
	    .otherwise({
		   redirectTo: '/' 
	    })
	    
	    $locationProvider.html5Mode(true);
	    
	}]);

})();