
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    modRewrite  = require('connect-modrewrite');

gulp.task('sass', function () {
    return gulp.src('assets/scss/*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass({
          outputStyle: 'compressed'
      }).on('error', sass.logError))
      .pipe(rename({ suffix: '.min' }))
      .pipe(sourcemaps.write('/maps/'))
      .pipe(gulp.dest('assets/css/'))
});


// watch and update
gulp.task('watch', function () {
    browserSync.init({
        server: {
           baseDir: './',
           middleware: [
                modRewrite([
                    '!\\.\\w+$ /index.html [L]'
                ])
            ]
        },
    });
    gulp.watch("assets/scss/breakpoints/*.scss", ['sass', browserSync.reload]);
    gulp.watch("index.html").on('change', browserSync.reload);
    gulp.watch("assets/json/*.json").on('change', browserSync.reload);
});


gulp.task('default', ['sass', 'watch', ]);
