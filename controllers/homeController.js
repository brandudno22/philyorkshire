(function(){
	
	angular.module('philYorkshire').controller('homeController',['$scope','$http', function($scope, $http){
			
			$scope.work = [];
			
			// Sending request to EmpDetails.php files
			$http.get('assets/json/workItems.json').success(function(data){
				
				// Stored the returned data into scope
				$scope.work = data;
			
			});
			
	}]);
	
})();