(function() {
	angular.module('philYorkshire').controller('singleWork',['$http','$routeParams', '$scope', function($http,$routeParams, $scope){

	  	//var param = $routeParams.slug
	  	//console.log(param);
	  
		$http.get('assets/json/workItems.json').success(function(data){
			
			angular.forEach(data, function(item) {
	          if (item.slug == $routeParams.slug) 
	            $scope.workItems = item;
	        });
			
		}); 
	  
		// flexslider
    
/*
		$('.flexslider').flexslider({
			directionNav: false
		});
*/
		
		
	}]);
	
})();