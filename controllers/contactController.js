(function() {
	angular.module('philYorkshire').controller('contactController', ['$scope', '$http', function($scope, $http) {
      $scope.master = {};

/*
      $scope.update = function(user) {
        $scope.master = angular.copy(user);
      };
*/
      
      $scope.submitForm = function() {
	      
        // Posting data to php file
        $http({
          method  : 'POST',
          url     : 'clone.php',
          data    : $scope.user, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
            if (data.errors) {
              // Showing errors.
              $scope.errorName = data.errors.name;
              $scope.errorUserName = data.errors.username;
              $scope.errorEmail = data.errors.email;
              $scope.errorMessage = data.errors.messagenew;
            } else {
              $scope.message = data.message;
              $scope.formSubmitted = true;
            }
          });
        };
      
    }]);

})();